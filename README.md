# Digital_Signature


Digital Certificates are used for secure communication between the two parties. In digital certification, we ensure that the people, who are using our apps or Services are securely communicating with each other and those people can be the individual consumers or businesses.

In Digital Certification, we use both Hashing and Asymmetric encryption to create the digital signatures. 

After encrypting the hash of data, we obtain a digital signature later, which is used for verification for the data.

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
In this project I have used 3 files.
Index.js-->for generating private key and public key and stored it in a separate files so that we don't need to generate it every time instead we can reuse it.
author.js-->for  adding a digital signature to the pdf using a private key
recipient.js-->for  verifying the document based on the public key shared by the sender

	
## Technologies
Project is created with:
* Node version: 14.16.1

<p align="center">
  <a href="https://nodejs.org/">
    <img
      alt="Node.js"
      src="https://nodejs.org/static/images/logo-light.svg"
      width="300"
      height="100"
    />
  </a>
</p>
	
## Setup
To run this project, install it locally using npm:

```
$ cd src
$ npm install .
$ node filename 
```
