const crypto = require("crypto")
const fs=require('fs')

//Generating public and private keys using RSA
var  NodeRSA  = require ('node-rsa') ;
var key = new NodeRSA({ b :512 } );
var publicKey = key.exportKey('public');
var privateKey= key.exportKey('private');

//Storing keys in a file so that no need to regenerate every time
fs.writeFile('publickey.txt',publicKey,function(err){
    if(err) throw err;
})
fs.writeFile('privatekey.txt',privateKey,function(err){
    if(err) throw err;
})