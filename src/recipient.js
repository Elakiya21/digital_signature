const fs = require('fs');
const crypto = require("crypto");
const express = require('express')
const app = express();

app.use(express.json());

const HASH_NAME = "SHA256";
const PUBLIC_KEY_PATH = "publickey.txt"

app.post("/", function (req, res) {

	//Receiving data from author
	const body = req.body;
	const data = body.data;
	const signature = body.signature;

	const publicKey = fs.readFileSync(PUBLIC_KEY_PATH, { encoding: 'utf-8', flag: 'r' });

	//Hashing the plain text with SHA256
	const verify = crypto.createVerify(HASH_NAME);
	verify.write(data)
	verify.end();

	let values = {};
	//Decrypting signature with publickey and verifying
	if (verify.verify(publicKey, signature, 'hex')) {
		values = {
			status: "success",
			message: "verified successfully"
		}
	} else {
		values = {
			status: "failure",
			message: "invalid content"
		}
	}
	res.write(JSON.stringify(values));
	res.send();
})

app.listen(5000, function () {
	console.log("Server is listening...")
})
