const crypto = require("crypto");
const fs = require('fs');
const pdfparse = require('pdf-parse');
const request = require('request');

const RECIPIENT_URL = 'http://127.0.0.1:5000';
const HASH_NAME = "SHA256";
const PDF_FILE_NAME = 'positivity.pdf';
const PRIVATE_KEY_NAME = "privatekey.txt";

const pdffile = fs.readFileSync(PDF_FILE_NAME);

pdfparse(pdffile).then((content) => {
	postContent(content.text)
})

const postContent = (data) => {
	const privateKey = fs.readFileSync(PRIVATE_KEY_NAME, {encoding:'utf-8', flag:'r'});

	//Hashing the plain text with SHA256
	const sign = crypto.createSign(HASH_NAME)
	sign.write(data);
	sign.end();

	//Generating Digital Signature 
	const signature = sign.sign(privateKey, 'hex');

	//Sending signature and data to recipient
	const values = {
		signature: signature,
		data: data
	}
	request.post(RECIPIENT_URL, {
		json: values
	}, (error, response, body) => {
		if (error) {
			console.error(error)
			throw error
		}
		console.log(body)
	})
}